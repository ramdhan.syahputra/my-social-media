//
//  HomeView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 19/02/24.
//

import SwiftUI
import PocketBase

struct HomeView: View {
    @State private var isLogout: Bool = false
    @State private var isLoading: Bool = false
    @State private var posts: [Post] = []
    @State private var isCreatePresented = false
    @Binding var path: NavigationPath
//    @EnvironmentObject var pb: PocketBase<User>
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    
    var body: some View {
        NavigationStack {
            ScrollView {
                if isLoading {
                    ProgressView()
                } else {
                    LazyVStack(spacing: 24) {
                        ForEach(posts) { item in
                            PostView(post: item)
                        }
                    }
                }
            }
            .padding(.zero)
            .navigationTitle("Story")
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button {
                        isCreatePresented.toggle()
                    } label: {
                        Image(systemName: "plus")
                    }
                    .foregroundStyle(.red)
                }
                
                ToolbarItem(placement: .topBarTrailing) {
                    Menu {
                        NavigationLink {
                            ProfileView()
                        } label: {
                            Label(
                                title: { Text("Profile") },
                                icon: { Image(systemName: "person.fill") }
                            )
                        }
                        
                        NavigationLink {
                            SettingsView()
                        } label: {
                            Label(
                                title: { Text("Settings") },
                                icon: { Image(systemName: "gear") }
                            )
                        }
                        
                        Button(role: .destructive) {
                            isLogout.toggle()
                        } label: {
                            Label(
                                title: { Text("Logout") },
                                icon: { Image(systemName: "rectangle.portrait.and.arrow.right.fill") }
                            )
                        }
                    } label: {
                        Image(systemName: "person")
                            .tint(.red)
                    }
                }
            }
            .confirmationDialog("Are you sure to logout?", isPresented: $isLogout, titleVisibility: .visible) {
                Button(role: .destructive) {
                    pb.authStore.clear()
                    UserDefaults.standard.setValue(false, forKey: "session")
                } label: {
                    Text("Logout")
                }
            }
            .sheet(isPresented: $isCreatePresented) {
                CreatePostView()
            }
        }
        .task {
            await subscribePosts()
            guard posts.isEmpty else { return }
            await fetchPosts()
        }
        .refreshable {
            if !posts.isEmpty { posts.removeAll() }
            await fetchPosts()
        }
        .onDisappear {
            pb.collection("posts").unsubscribe()
        }
    }
    
    func fetchPosts() async {
        Task {
            isLoading = true
              
            // Get posts
            posts += await pb.collection("posts").getFullList(batch: 5, sort: "-created", expand: "user")
            
            debugPrint(posts.count)
              
            isLoading = false
        }
    }
    
    func subscribePosts() async {
        Task {
            pb.collection("posts").subscribe("*") { dict in
                if let result: Event<Post> = try? Utils.dictionaryToStruct(dictionary: dict ?? [:]) {
                    switch result.action {
                    case .create:
                        Task {
                            debugPrint("USER: \(result.record.user)")
                            
                            if let user: User = await pb.collection("users").getOne(id: result.record.user) {
                                
                                var post = result.record
                                post.expand = PostExpand(user: user)
                                
                                debugPrint("Post avatar : \(user.avatar)")
                                debugPrint("Post expand : \(post.expand)")
                                
                                withAnimation {
                                    posts.insert(post, at: 0)
                                }
                            }
                        }
                    case .update:
                        if let row = self.posts.firstIndex(where: { post in
                            post.id == result.record.id
                        }) {
                            Task {
                                if let user: User = await pb.collection("users").getOne(id: result.record.user) {
                                    
                                    var post = result.record
                                    post.expand = PostExpand(user: user)
                                    
                                    debugPrint("Post avatar : \(user.avatar)")
                                    debugPrint("Post expand : \(post.expand)")
                                    
                                    withAnimation {
                                        posts[row] = post
                                    }
                                }
                            }
                        }
                    case .delete:
                        posts = posts.filter({ post in
                            post.id != result.record.id
                        })
                    }
                }
            }
        }
    }
}

#Preview {
    HomeView(path:.constant(NavigationPath())).environmentObject(PocketBase<User>())
}
