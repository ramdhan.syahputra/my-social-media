//
//  CreatePostView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 22/02/24.
//

import SwiftUI
import PhotosUI
import PocketBase

struct CreatePostView : View {
    @StateObject private var viewModel = CreatePostViewModel()
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                TextField("Content", text: $viewModel.content, axis: .vertical)
                    .lineLimit(5...20)
                    .padding(24)
                
                if viewModel.photos.isEmpty {
                    Button {
                        viewModel.isPhotosPickerPresented.toggle()
                    } label: {
                        VStack(alignment: .center) {
                            Image(systemName: "photo")
                                .resizable()
                                .scaledToFill()
                                .foregroundStyle(.gray)
                                .frame(width: 100, height: 100)
                                .padding([.horizontal, .top], 32)
                                .padding(.bottom, 8)
                            
                            Text("Add photos")
                                .font(.footnote)
                                .foregroundStyle(.gray)
                        }
                        .frame(maxWidth: .infinity, maxHeight: 200, alignment: .center)
                        .padding()
                        .background(.thinMaterial)
                    }
                } else {
                    TabView {
                        ForEach(viewModel.photos, id: \.self) { photo in
                            Image(uiImage: photo)
                                .resizable()
                                .scaledToFill()
                                .foregroundStyle(.gray)
                        }
                    }
                    .frame(height: 200)
                    .frame(maxWidth: .infinity)
                    .tabViewStyle(.page)
                    Button {
                        viewModel.isPhotosPickerPresented.toggle()
                    } label: {
                        Text("Change photos")
                    }
                    .padding([.horizontal, .top])
                }
                
                Spacer()
            }
            .overlay(alignment: .center) {
                if viewModel.isLoading {
                    ProgressView()
                        .padding()
                        .background(.thinMaterial)
                        .clipShape(.rect(cornerRadius: 16))
                }
            }
            .toolbar {
                ToolbarItem {
                    Button {
                        viewModel.createPost()
                    } label: {
                        Image(systemName: "plus.circle")
                            .foregroundStyle(.blue)
                    }
                }
                
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        dismiss()
                    } label: {
                        Text("Cancel")
                            .foregroundStyle(.red)
                    }
                }
            }
            .alert("Alert", isPresented: $viewModel.isAlertPresented, actions: {
                
            }, message: {
                Text(viewModel.alertMessage)
            })
            .onChange(of: viewModel.isDone, { oldValue, newValue in
                if (newValue) {
                    dismiss()
                    viewModel.dispose()
                }
            })
            .navigationTitle("Create new post")
            .photosPicker(isPresented: $viewModel.isPhotosPickerPresented, selection: $viewModel.photoSelections)
        }
    }
}

#Preview {
    CreatePostView()
//        .environmentObject(CreatePostViewModel(pb: PocketBase<User>()))
}
