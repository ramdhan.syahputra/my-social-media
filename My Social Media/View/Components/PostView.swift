//
//  PostView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 19/02/24.
//

import SwiftUI
import PocketBase

struct PostView: View {
    var imageUrl = "https://img.freepik.com/free-psd/3d-illustration-person-with-sunglasses_23-2149436188.jpg"
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    var post: Post
    @State var isProfile = false
    var callback: ((String?) -> Void)?
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                AsyncImage(url: pb.getFileUrl(post.expand?.user?.id ?? "", "users", post.expand?.user?.avatar ?? "")) { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .frame(width: 30, height: 30)
                        .clipShape(.circle)
                } placeholder: {
                    ProgressView()
                        .frame(width: 30, height: 30)
                }
                
                Text("@\(post.expand?.user?.username ?? "anonymous")")
                    .font(.caption)
                
                Spacer()
                
                Text(post.created ?? "19 - feb - 2024")
                    .font(.caption)
                    .foregroundStyle(.secondary)
            }
            .padding(.bottom, 8)
            .padding(.horizontal, 12)
            
            if !post.images.isEmpty, (post.id != nil) {
                GeometryReader {geo in
                    let size = geo.size
                    
                    TabView {
                        ForEach(post.images, id: \.self) { image in
                            AsyncImage(url: post.id != nil ? pb.getFileUrl(post.id ?? "", "posts", image) : URL(string: imageUrl)) { image in
                                image
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: size.width)
                            } placeholder: {
                                ProgressView()
                                    .frame(maxWidth: .infinity)
                            }
                        }
                    }
                    .tabViewStyle(.page)
                }
                .frame(height: 250)
                .padding(.bottom, 12)
                .contextMenu(ContextMenu(menuItems: {
                    if isProfile {
                        Button(role: .destructive) {
//                            removePost(id: post.id)
                            callback!(post.id)
                        } label: {
                            Label {
                                Text("Remove")
                            } icon: {
                                Image(systemName: "eraser")
                            }
                        }
                        
                        Button {
                            
                        } label: {
                            Label {
                                Text("Edit")
                            } icon: {
                                Image(systemName: "pencil.circle")
                            }
                        }
                    }
                }))
            }
            
            HStack {
                Button(action: likePost) {
                    VStack {
                        Image(systemName: post.liked.contains(pb.authStore.model?.id ?? "") ? "heart.fill" : "heart")
                    }
                }
                
                Button {
                    
                } label: {
                    Image(systemName: "message")
                }
                
                Button {
                    
                } label: {
                    Image(systemName: "bookmark")
                }
            }
            .padding(.horizontal, 12)
            .padding(.bottom, 2)
            
            Text(String(post.liked.count))
                .font(.caption2)
                .bold()
                .padding(.horizontal, 12)
                .padding(.bottom, 4)
            
            Text(post.text)
                .padding(.horizontal, 12)
                .font(.caption)
                .padding(.bottom, 12)
            
            Divider()
        }
        .onAppear {
            debugPrint("EXPAND : \(post.expand)")
        }
    }
    
    private func likePost() {
        Task {
            let record = await pb.collection("posts").update(post.id!, body: [post.liked.contains(pb.authStore.model?.id ?? "") ? "liked-" : "liked+" : pb.authStore.model?.id ?? ""])
            
            if let error = try? ErrorResponse(dictionary: record) {
                debugPrint("Like post error. \(error.message)")
            }
        }
    }
}

#Preview {
    PostView(post: Post.mockPosts.first!)
        .environmentObject(PocketBase<User>())
}
