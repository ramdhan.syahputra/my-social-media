//
//  SettingsView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 22/02/24.
//

import SwiftUI
import PocketBase

struct SettingsView: View {
    @State private var isConfirmationPresented = false
//    @EnvironmentObject var pb: PocketBase<User>
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    
    var body: some View {
        NavigationStack {
            Form {
                Image(systemName: "person.fill")
                    .resizable()
                    .frame(width: 55, height: 55)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding()
                
                Section("Account Information") {
                    Label {
                        Text(pb.authStore.model?.name ?? "Loremipsum")
                    } icon: {
                        Image(systemName: "person")
                            .foregroundStyle(.red)
                    }
                    
                    Label {
                        Text("@\(pb.authStore.model?.username ?? "")")
                    } icon: {
                        Image(systemName: "number.circle")
                            .foregroundStyle(.red)
                    }
                }
                
                Section("Account created") {
                    Label {
                        Text(pb.authStore.model?.created ?? "--")
                    } icon: {
                        Image(systemName: "clock")
                            .foregroundStyle(.red)
                    }
                }
                
                Button {
                    isConfirmationPresented.toggle()
                } label: {
                    Text("Remove account")
                }
                .buttonStyle(.borderless)
                .foregroundStyle(.red)
                
                
            }
            .navigationTitle("Settings")
        }
        .confirmationDialog("Do you want to delete your account?", isPresented: $isConfirmationPresented, titleVisibility: .visible) {
            Button(role: .destructive) {
                Task {
                    guard let userId = pb.authStore.model?.id else {
                        return
                    }
                    
                    if let result = await pb.collection("users").delete(userId) {
                     
                        debugPrint(result)
                    }
                    
                    pb.authStore.clear()
                    UserDefaults.standard.setValue(false, forKey: "session")
                }
            } label: {
                Text("Delete account")
            }
        }
    }
}

#Preview {
    SettingsView()
        .environmentObject(PocketBase<User>())
}
