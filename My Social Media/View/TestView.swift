//
//  TestView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 13/03/24.
//

import SwiftUI

struct TestView: View {
    @State private var path = NavigationPath()
    
    var body: some View {
        NavigationStack {
            List {
                NavigationLink(value: PathEnum.home) {
                    Text("Halaman pertama")
                }
                
                NavigationLink(value: PathEnum.login) {
                    Text("Halaman kedua")
                }
                
                NavigationLink(value: PathEnum.register) {
                    Text("Halaman ketiga")
                }
            }
            .listStyle(.plain)
            .navigationTitle("Test View")
            .navigationDestination(for: PathEnum.self) { data in
                switch (data) {
                case .home:
                    Text("Home")
                case .login:
                    Text("Login")
                case .settings:
                    Text("Settings")
                case .profile:
                    Text("Profile")
//                case .register:
//                    Text("Register")
                default:
                    Text("Default")
                }
            }
            
        }
    }
}

#Preview {
    TestView()
}
