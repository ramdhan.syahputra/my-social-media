//
//  ContentView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 16/02/24.
//

import SwiftUI
import PocketBase

struct LoginView: View {
    @State private var username = ""
    @State private var password = ""
    @State private var isRegisterOpened = false
    @State private var isAlertPresented = false
    @State private var isErrorPresented = false
    @State private var isLoading = false
    @State private var errorMessage = "Ooopsss, username or password is wrong!"
    @AppStorage("session") var isLoggedIn: Bool = false
//    @EnvironmentObject var client: PocketBase<User>
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    @Binding var path: NavigationPath
    
    var body: some View {
        ZStack(alignment: .center) {
            VStack(alignment: .leading) {
                Text("My Social Media App")
                    .font(.largeTitle)
                    .bold()
                    .padding(.horizontal)
                    .padding(.top, 24)
                
                Text("Welcome to my social media, please login with your account..")
                    .font(.subheadline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                    .padding(.bottom, 6)
                
                TextField("Username", text: $username)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                SecureField("Password", text: $password)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                Button {
                    if username.isEmpty || password.isEmpty {
                        isAlertPresented.toggle()
                        errorMessage = "Ooopsss, please fill username or password first!"
                        return
                    }
                    
                    loginUser()
                } label: {
                    Text("Login")
                        .bold()
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(.red)
                        .foregroundColor(.white)
                        .clipShape(.rect(cornerRadius: 12))
                        .padding()
                }
                
                Button {
                    debugPrint("Forgot password pressed")
                } label: {
                    Text("Forgot password?")
                        .font(.footnote)
                        .bold()
                        .frame(maxWidth: .infinity, alignment: .trailing)
                        .padding(.horizontal)
                }
                
                Text("Dont have an account?")
                    .font(.footnote)
                    .frame(maxWidth: .infinity)
                    .padding(.top, 12)
                    .padding(.bottom, 4)
                
                Button {
                    path.append(PathEnum.register)
                } label: {
                    Text("Register")
                        .font(.footnote)
                        .bold()
                        .foregroundStyle(.blue)
                        .frame(maxWidth: .infinity)
                }
                
                Spacer()
                
                Text("©Copy right by AQI")
                    .font(.caption2)
                    .bold()
                    .italic()
                    .foregroundStyle(.secondary)
                    .frame(maxWidth: .infinity)
                
            }
            
            if isLoading {
                VStack {
                    ProgressView()
                        .padding(.bottom, 6)
                    Text("Loading")
                }
                .padding()
                .background(.thinMaterial)
                .clipShape(.rect(cornerRadius: 12))
            }
        }
//        .fullScreenCover(isPresented: $isRegisterOpened, content: {
//            RegisterView(isRegisterOpened: $isRegisterOpened)
//        })
//        .fullScreenCover(isPresented: $isLoggedIn, content: {
//            HomeView(path: $path)
//        })
        .alert(isPresented: $isAlertPresented) {
            Alert(title: Text("Alert!"), message: Text(errorMessage))
        }
    }
    
    func loginUser() {
        isLoading = true
        Task {
          if let dict = await pb.collection("users").authWithPassword(username, password) {
              
            if let error = try? ErrorResponse(dictionary: dict) {
                let errString = "\(error.message)\n\(error.data.first?.value.message ?? "")"
                debugPrint("error: \(errString)")
                
                isLoading = false
                isAlertPresented = true
            } else {
              // MARK: Login Successful
              isLoading = false
              isLoggedIn = true
//                path = NavigationPath()
//                path.append(PathEnum.home)
            }
          }
        }
  }
}

#Preview {
    LoginView(path: .constant(NavigationPath()))
}
