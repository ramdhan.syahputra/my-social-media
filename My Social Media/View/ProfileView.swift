//
//  ProfileView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 08/03/24.
//

import SwiftUI
import PocketBase

struct ProfileView: View {
    private let url = URL(string: "https://ew.com/thmb/qH6e1QIMvbT-yPg6xn7eJ3hKEQI=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/simp_homersingle08_f_hires2-2000-cf09d1b1345c4e66b57bced2bebbe492.jpg")
    
//    @EnvironmentObject var pb: PocketBase<User>
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    @State private var posts = [Post]()
    @State private var isLoading = false
    
    var body: some View {
        NavigationStack {
            ScrollView {
                if isLoading {
                    ProgressView()
                        .padding()
                        .background(.thinMaterial)
                        .clipShape(.rect(cornerRadius: 12))
                } else {
                    AsyncImage(url: pb.getFileUrl(pb.authStore.model?.id ?? "", "users", pb.authStore.model?.avatar ?? "")) { image in
                        image
                            .image?
                            .resizable()
                            .scaledToFill()
                            .frame(width: 150, height: 150)
                            .clipShape(.circle)
                    }
                    .overlay {
                        Circle()
                            .stroke(.red, lineWidth: 3)
                    }
                    .overlay(alignment: .bottomTrailing) {
                        Button {
                            
                        } label: {
                            Image(systemName: "pencil.circle")
                                .resizable()
                                .frame(width: 35, height: 35)
                                .background(.white)
                                .clipShape(.circle)
                        }
                    }
                    .padding(.bottom, 12)
                    
                    Text(pb.authStore.model?.name ?? "Unknown")
                        .font(.headline)
                    
                    Text("@\(pb.authStore.model?.username ?? "Unknown")")
                        .font(.footnote)
                        .padding(.bottom, 12)
                    
                    Divider()
                        .padding(.bottom, 12)
                    
                    Text("Your post")
                        .font(.headline)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    ForEach(posts) { post in
                        PostView(post: post, isProfile: true) { postId in
                            removePost(id: postId)
                            debugPrint("INI TEST")
                        }
                    }
                }
            }
            .navigationTitle("Profile")
        }
        .task {
            fetchPosts()
        }
        .refreshable {
            fetchPosts()
        }
    }
    
    private func removePost(id: String?) {
        guard let id = id else {
            debugPrint("id is nil")
            return
        }
        
        Task {
            await pb.collection("posts").delete(id)
            fetchPosts()
        }
    }
    
    private func fetchPosts() {
        isLoading = true
        
        Task {
            guard let username = pb.authStore.model?.id else {
                debugPrint("username doesn't exist")
                isLoading = false
                return
            }
            
            let posts: [Post] = await pb.collection("posts").getFullList(filter: "user = '\(username)'", sort: "-created", expand: "user")
            
            self.posts = posts
            
            debugPrint("Jumlah posts profile view: \(posts.count)")
            
            isLoading = false
        }
    }
}

#Preview {
    ProfileView()
        .environmentObject(PocketBase<User>())
}
