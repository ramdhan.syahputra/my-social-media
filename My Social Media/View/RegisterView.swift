//
//  RegisterView.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 16/02/24.
//

import SwiftUI
import PocketBase

struct RegisterView: View {
    @State private var username = ""
    @State private var name = ""
    @State private var email = ""
    @State private var password = ""
    @State private var rePassword = ""
    @State private var isAlertPresented = false
    @State private var isLoading = false
    @State private var errorMessage = "Oopsss! please fill the form first!"
    @Environment(\.dismiss) private var dismiss
//    @Binding var isRegisterOpened: Bool
//    @EnvironmentObject var client: PocketBase<User>
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
    @AppStorage("session") var isLoggedIn: Bool = false
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                Text("Register new account")
                    .font(.largeTitle)
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .frame(height: 12)
                    .padding(.horizontal)
                    .padding(.top, 24)
                    .padding(.bottom, 8)
                
                Text("Please fullfil the following form to create your account.")
                    .font(.subheadline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                    .padding(.top, 6)
                    .padding(.bottom, 6)
                
                TextField("Username", text: $username)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                TextField("Name", text: $name)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                TextField("Email", text: $email)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                SecureField("Password", text: $password)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                SecureField("Re-Password", text: $rePassword)
                    .padding()
                    .background(.thinMaterial)
                    .clipShape(.rect(cornerRadius: 12))
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                
                Button {
                    if (email.isEmpty || name.isEmpty || username.isEmpty || password.isEmpty || rePassword.isEmpty) {
                        isAlertPresented.toggle()
                        return
                    }
                    
                    registerUser()
                } label: {
                    Text("Register")
                        .font(.footnote)
                        .bold()
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(.red)
                        .foregroundColor(.white)
                        .clipShape(.rect(cornerRadius: 12))
                        .padding([.top, .horizontal])
                }
                
                Button {
//                    isRegisterOpened.toggle()
                    dismiss()
                } label: {
                    Text("Back to login")
                        .font(.footnote)
                        .bold()
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(.thinMaterial)
                        .clipShape(.rect(cornerRadius: 12))
                        .padding()
                }
                
                Spacer()
            }
                
            if isLoading {
                VStack {
                    ProgressView()
                        .padding(.bottom, 6)
                    Text("Loading")
                }
                .padding()
                .background(.thinMaterial)
                .clipShape(.rect(cornerRadius: 12))
            }
        }
        .alert(isPresented: $isAlertPresented) {
            Alert(title: Text("Alert!"), message: Text(errorMessage))
        }
    }
    
    func registerUser() {
        isLoading = true
        Task {
          let request = RegisterRequest(
            username: username,
            email: email,
            name: name,
            password: password,
            passwordConfirm: rePassword
          )
          
          // MARK: Create User
          if let record = await pb.collection("users").create(request) {
            if let err = try? ErrorResponse(dictionary: record) {
                let errString = "\(err.message)\n\(err.data.first?.value.message ?? "")"
                errorMessage = errString
                isAlertPresented.toggle()
                isLoading.toggle()
            } else {
              // MARK: Send Request Verification
              if let dict = await pb.collection("users").requestVerification(email) {
                if let err = try? ErrorResponse(dictionary: dict) {
                    errorMessage = err.message
                    isAlertPresented.toggle()
                    isLoading.toggle()
                }
              } else {
                // MARK: Create User Successful
                // MARK: Let's Login
                  loginUser()
              }
            }
          }
        }
      }
    
    func loginUser() {
        isLoading = true
        Task {
          if let dict = await pb.collection("users").authWithPassword(username, password) {
              
            if let error = try? ErrorResponse(dictionary: dict) {
                let errString = "\(error.message)\n\(error.data.first?.value.message ?? "")"
                debugPrint("error: \(errString)")
                
                isLoading = false
                isAlertPresented = true
            } else {
              // MARK: Login Successful
              isLoading = false
              isLoggedIn = true
            }
          }
        }
  }
}

#Preview {
//    RegisterView(isRegisterOpened: .constant(false))
    RegisterView()
}

fileprivate struct RegisterRequest: Codable, MultipartFormData {
    var username: String
    var email: String
    var emailVisibility: Bool = false
    var name: String
    var password: String
    var passwordConfirm: String
}
