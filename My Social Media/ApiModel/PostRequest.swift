//
//  PostRequest.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 05/03/24.
//

import Foundation
import PocketBase

struct PostRequest : Codable, Identifiable, MultipartFormData {
    var id: String?
    var user: String?
    var text: String?
    var images: [File]?
    var liked: String?
}
