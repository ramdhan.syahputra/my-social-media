//
//  CreatePostViewModel.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 08/03/24.
//

import Foundation
import UIKit
import _PhotosUI_SwiftUI
import PocketBase

@MainActor
class CreatePostViewModel : ObservableObject {
    @Published private(set) var photos : [UIImage] = []
    @Published var photoSelections : [PhotosPickerItem] = [] {
        didSet {
            isLoading.toggle()
            setPhotos(selections: photoSelections)
            isLoading.toggle()
        }
    }
    @Published var photosData: [Data] = []
    
    @Published var content: String = ""
    @Published var isPhotosPickerPresented = false
    
    @Published var isLoading = false
    @Published var isAlertPresented = false
    @Published var alertMessage = ""
    @Published var isDone = false
    
    let pb = PocketBase<User>(host: "http://127.0.0.1:8090/_/")
    
//    init(pb: PocketBase<User>) {
//        self.pb = pb
//    }
    
    private func setPhotos(selections: [PhotosPickerItem]) {
        if selections.isEmpty {
            debugPrint("Photo selection is empty")
            return
        }
        
        Task {
            for selection in selections {
                guard let data = try? await selection.loadTransferable(type: Data.self) else {
                    debugPrint("Convert to data is error")
                    return
                }
                
                photosData.append(data)
                
                guard let uiImage = UIImage(data: data) else {
                    debugPrint("Convert to UIImage is error")
                    return
                }
                
                // Will be executed in Main Thread Automatically if we use @MainActor annotation
                photos.append(uiImage)
            }
        }
    }
    
    func createPost() {
        isLoading = true
        
        if photosData.isEmpty || content.isEmpty {
            isAlertPresented = true
            alertMessage = "Please add your photos and fill your content"
            isLoading = false
            return
        }
        
        Task {
            var files = [File]()
            
            for data in photosData {
                files.append(File(mimeType: "image/jpeg", filename: "post_image\(Int.random(in: 0...999))", data: data))
            }
            
            guard let userId = pb.authStore.model?.id else {
                debugPrint("User id doesnt exist")
                isAlertPresented = true
                alertMessage = "User id doesnt exist"
                isLoading = false
                return
            }
            
            let request = PostRequest(user: userId, text: content, images: files.isEmpty ? nil : files)
            
            guard let record = await pb.collection("posts").create(request) else {
                debugPrint("Post data is failed")
                isAlertPresented = true
                alertMessage = "Post data is failed"
                isLoading = false
                return
            }
            
            if let errorResponse = try? ErrorResponse(dictionary: record) {
                debugPrint("Error: \(errorResponse)")
                isAlertPresented = true
                alertMessage = "Post data response is failed : \(errorResponse)"
                isLoading = false
                return
            }
            
            isLoading.toggle()
            isDone = true
        }
    }
    
    func dispose() {
        photos.removeAll()
        photosData.removeAll()
        photoSelections.removeAll()
        content.removeAll()
        isDone = false
    }
}
