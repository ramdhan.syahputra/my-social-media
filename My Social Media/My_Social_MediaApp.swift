//
//  My_Social_MediaApp.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 16/02/24.
//

import SwiftUI
import PocketBase

@main
struct My_Social_MediaApp: App {
    @AppStorage("session") private var isLoggedIn: Bool = false
//    @State private var client: PocketBase<User>?
//    @State private var viewModel: CreatePostViewModel?
    @State private var path = NavigationPath()
    
//    init() {
//        let pb = PocketBase<User>(host: "http://127.0.0.1:8090")
//        self.client = pb
//        self.viewModel = CreatePostViewModel(pb: pb)
//    }
    
    var body: some Scene {
        WindowGroup {
            NavigationStack(path: $path) {
                if isLoggedIn {
                    HomeView(path: $path)
                        .navigationDestination(for: PathEnum.self) { destination in
                            if destination == .profile {
                                ProfileView()
                            }
                            
                            if destination == .settings {
                                SettingsView()
                            }
                            
                            if destination == .login {
                                LoginView(path: $path)
                            }
                        }
                } else {
                    LoginView(path: $path)
                        .navigationDestination(for: PathEnum.self) { destination in
                            if destination == .register {
//                                RegisterView(isRegisterOpened: .constant(false))
                                RegisterView()
                            }
                            
                            if destination == .home {
                                HomeView(path: $path)
                            }
                            
                            if destination == .login {
                                LoginView(path: $path)
                            }
                        }
                }
            }
//            TestView()
        }
    }
}
