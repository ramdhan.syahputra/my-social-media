//
//  PathEnum.swift
//  My Social Media
//
//  Created by M Ramdhan Syahputra on 13/03/24.
//

import Foundation

enum PathEnum: String, Hashable {
    case home, login, register, profile, settings
}
